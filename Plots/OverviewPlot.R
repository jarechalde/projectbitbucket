library(ggplot2)

#Setting the working directory
#setwd('C:/Users/jarec/Documents/GitHub/ProjectBitbucket/Data')
setwd('/home/javier/Work/projectbitbucket/Data')
getwd()

#We read the data
mydata = read.csv('2000-2015.csv', header = TRUE, sep = ",")
mydata$Admit_Term<-factor(mydata$Admit_Term)

#Top Schools
mysch<-summary(mydata$schoolname)[1:5]
schls<-names(mysch)

years<-levels(mydata$Admit_Term)

cyear<-c()
cschool<-c()
ccomp<-c()
ctag<-c()

for (i in 1:length(years)){
  year<-years[i]
  dfy<-mydata[mydata$Admit_Term == year,]
  for (j in 1:length(schls)){
    school<-schls[j]
    dfschool<-dfy[dfy$schoolname == school,]
    
    completed<-length(dfschool$Completion[dfschool$Completion == 1])
    dropped<-length(dfschool$Completion[dfschool$Completion == 0])
    
    crate<-(completed/(dropped+completed))
    
    cyear<-c(cyear,year,year)
    cschool<-c(cschool,school,school)
    ccomp<-c(ccomp,completed,dropped)
    ctag<-c(ctag,'Completed','Dropped')

  }
}

newdf0<-data.frame(cyear,cschool,ccomp,ctag)
names(newdf0)<-c('Term','HSchool','Completion','Tag')

ggplot(newdf0,aes(Term,Completion)) + 
geom_bar(stat = 'identity', aes(fill = HSchool), position = 'dodge') + 
ggtitle('Admitted Students') +
theme(plot.title = element_text(hjust = 0.5)) +
xlab('Term') + ylab('Students') +
theme_bw(base_size = 20) +
theme(plot.title = element_text(hjust = 0.5)) +
facet_grid(~Admit_Term)

#Students by term
schools<-levels(mydata$ACAD_GROUP_A)
years<-levels(factor(mydata$Admit_Term))

cyear<-c()
cschool<-c()
ctotal<-c()

for (i in 1:length(years)){
  year<-years[i]
  
  for (j in 1:length(schools)){
    school<-schools[j]
    
    dfst<-mydata$ACAD_GROUP_A[mydata$Admit_Term == year]
    dfst<-dfst[dfst == school]
    
    totstud<-length(dfst)
    print(totstud)
    
    cyear<-c(cyear,year)
    cschool<-c(cschool,school)
    ctotal<-c(ctotal,totstud)
    
  }
  
}

newdf<-data.frame(cyear,cschool,ctotal)
names(newdf)<-c('Term','College','Total')



ggplot(newdf,aes(Term,Total)) + 
  geom_bar(stat = 'identity', aes(fill = College), position = 'stack') + 
  ggtitle('Admitted Students') +
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab('Term') + ylab('Students') +
  theme_bw(base_size = 20) +
  theme(plot.title = element_text(hjust = 0.5))

#COMPLETION RATE
cyear<-c()
ctag<-c()
ccomp<-c()

for (i in 1:length(years)){
  year<-years[i]
  
  for (j in 0:1){
    
    if (j == 0){
      
      dfst<-mydata$Completion[mydata$Admit_Term == year]
      dfst<-dfst[dfst == j]
      comp0<-length(dfst)
      
      cyear<-c(cyear,year)
      
    }
    
    if (j == 1){
      
      dfst<-mydata$Completion[mydata$Admit_Term == year]
      tcomp<-length(dfst)
      dfst<-dfst[dfst == j]
      comp1<-length(dfst)
      
      cyear<-c(cyear,year)
      
    }
    
    
  }
  
  comp0<-(comp0/tcomp)*100
  comp1<-(comp1/tcomp)*100
  
  ccomp<-c(ccomp,comp0,comp1)
  ctag<-c(ctag,'Dropped/Transferred','Completed')
  
}

newdf<-data.frame(cyear,ccomp,ctag)
names(newdf)<-c('Term','Comp','Tag')


ggplot(newdf,aes(Term,Comp)) + 
geom_bar(stat = 'identity', aes(fill = Tag), position = 'stack') + 
ggtitle('Completion rate') +
theme(plot.title = element_text(hjust = 0.5)) +
xlab('Term') + ylab('%') +
theme_bw(base_size = 20) +
theme(plot.title = element_text(hjust = 0.5)) +
scale_fill_manual(values=c('lightgreen', 'indianred1'))

#Location States
states<-levels(mydata$schoolstate)

ctotal<-c()
cstate<-c()

for (i in 1:length(states)){
  state<-states[i]
  
  tstate<-length(mydata$ID[mydata$schoolstate == state])
  
  if (state == 'MA'){
    state = ' Massachusetts'
  }
  
  else{
    state = 'Outside MA'
  }
  
  ctotal<-c(ctotal,tstate)
  cstate<-c(cstate,state)
}

dfstate<-data.frame(cstate,ctotal)
names(dfstate)<-c('State','Total')

#Out of state 
ggplot(dfstate, aes(x="", y=Total, fill=State)) +
geom_bar(width = 1, stat = "identity") + 
coord_polar("y", start=0) +
ggtitle('Origin of Students') +
theme_void(base_size = 20) +
theme(plot.title = element_text(hjust = 0.5))

#County
datacounty<-mydata[mydata$schoolstate == 'MA',]
datacounty$Schoolcounty<-factor(datacounty$Schoolcounty)

counties<-summary(datacounty$Schoolcounty)
counties<-counties[counties>20]
counties<-names(counties)

ctotal<-c()
ccount<-c()

for (i in 1:length(counties)){
  county<-counties[i]
  
  if(county == ''){
    next()
  }
  
  tcount<-length(mydata$ID[datacounty$Schoolcounty == county])
  
  ctotal<-c(ctotal,tcount)
  ccount<-c(ccount,county)
}

dfstate<-data.frame(ccount,ctotal)
names(dfstate)<-c('County','Total')

#Counties
ggplot(dfstate, aes(x="", y=Total, fill=County)) +
geom_bar(width = 1, stat = "identity") + 
coord_polar("y", start=0) +
ggtitle('Origin of Students (MA)') +
theme_void(base_size = 20) +
theme(plot.title = element_text(hjust = 0.5))
