#Loading libraries
library(randomForest)
library(caret)
library(futile.logger)
library(ROCR)

#Setting the working directory
#setwd('C:/Users/jarec/Documents/GitHub/ProjectBitbucket/Data')
setwd('/home/javier/Work/projectbitbucket/Data')
getwd()

#File to append the logs
flog.appender(appender.file('logsRF.txt'))
flog.info('1000 Trees, mtry = 2')

#We read the data
data = read.csv('DataFeatures_U_Zip_NONA.csv', header = TRUE, sep = ",")


#data<-data[data$MajorType=='BA',]

#Adding column that says if the student is from Bristol county or not
data$Bristol<-ifelse(data$Schoolcounty == 'Bristol', 'Yes', 'No')

#Try this out
maxrank<-max(as.integer(data$School_rank))
for (i in 1:length(data$School_rank)){
  #print('----------')
  rank<-as.integer(data$School_rank[i])
  rank<-(rank-1)/(maxrank-1)
  if(rank<=3/10){
    data$School_rank[i]<-'1'
  }
  
  else if(rank>3/10 && rank<7/10){
    data$School_rank[i]<-'2'
  }
  
  else{
    data$School_rank[i]<-'3'
  }
  
}

#Fixing data
#data$Admit_Term<-factor(as.character(data$Admit_Term))
data$Location<-factor(as.character(data$Location))
data$Honors<-factor(as.character(data$Honors))
data$School_rank<-factor(as.character(data$School_rank))

data$Above_GPA<-factor(as.character(data$Above_GPA))
data$Above_SAT<-factor(as.character(data$Above_SAT))
data$Above_HSGPA<-factor(as.character(data$Above_HSGPA))
data$Above_HSSAT<-factor(as.character(data$Above_HSSAT))

data$Perf_Eng_GPA<-factor(as.character(data$Perf_Eng_GPA))
data$Perf_Eng_SATMA<-factor(as.character(data$Perf_Eng_SATMA))
data$Perf_Eng_SATVB<-factor(as.character(data$Perf_Eng_SATVB))

data$Perf_Art_GPA<-factor(as.character(data$Perf_Art_GPA))
data$Perf_Art_SATMA<-factor(as.character(data$Perf_Art_SATMA))
data$Perf_Art_SATVB<-factor(as.character(data$Perf_Art_SATVB))

data$Perf_Nur_GPA<-factor(as.character(data$Perf_Nur_GPA))
data$Perf_Nur_SATMA<-factor(as.character(data$Perf_Nur_SATMA))
data$Perf_Nur_SATVB<-factor(as.character(data$Perf_Nur_SATVB))

data$Perf_Bus_GPA<-factor(as.character(data$Perf_Bus_GPA))
data$Perf_Bus_SATMA<-factor(as.character(data$Perf_Bus_SATMA))
data$Perf_Bus_SATVB<-factor(as.character(data$Perf_Bus_SATVB))

data$Perf_Other_GPA<-factor(as.character(data$Perf_Other_GPA))
data$Perf_Other_SATMA<-factor(as.character(data$Perf_Other_SATMA))
data$Perf_Other_SATVB<-factor(as.character(data$Perf_Other_SATVB))

data$Completion<-factor(as.character(data$Completion))

data$zipcode<-factor(as.character(data$zipcode))

what<-2
startyear<-2006
endyear<-2011

#Array for results
cacc<-c()
cyear<-c()

for (i in startyear:endyear){
  year<-i-what
  predyear<-i
  #Partitioning into training and test set
  #data<-data[data$ACAD_GROUP_A!='CAS',]
  datanew<-data[which(data$Admit_Term>=year & data$Admit_Term<=predyear),]
  train<-datanew[datanew$Admit_Term!=predyear,]
  test<-datanew[datanew$Admit_Term==predyear,]
  
  #Training the model
  myrf<-randomForest(Completion~
                    Admit_Term+
                    Gender+
                    hsgpa+SATMA+SATVB+Total_SAT+ #Data from the student
                    ACAD_GROUP_A+
                    MajorType+
                    Changed_Major+
                    Above_GPA+Above_SAT+Above_HSGPA+Above_HSSAT+
                    Honors+
                    Location+ #Seems to be very important
                    D_Adm+
                    Distance2+
                    School_SAT+School_GPA+
                    Percentage+Poverty+Population+Unemployment+Income2 #Data from ACS seems to be very important
                  , data = train, ntree = 500, mtry = 2)

  
  
  
  #Prediction
  pred<-predict(myrf,test, type = 'prob')
  pred<-pred[,2]
  predeval<-prediction(pred,test$Completion)
  
  eval<-performance(predeval,'acc')
  
  if (FALSE){
  
  roc<-performance(predeval,'tpr','fpr')
  plot(roc,main = paste('ROC CURVE ', as.character(predyear), ' COHORT'), xlab = 'Sensitivity',ylab = '1-Specificity')
  abline(a = 0, b = 1, col = 'red')
  #plot(eval,xlim = c(0,1), ylim = c(0,1))
  
  #AREA UNDER CURVE
  auc<-performance(predeval, 'auc')
  auc<-unlist(auc@y.values)
  auc<-round(auc,4)
  legend(.6,.2, auc, title = 'AUC')
  }
  
  
  pred<-ifelse(pred>0.5,1,0)
  pred<-factor(pred)
  
  #Confusion Matrix
  cmat<-confusionMatrix(pred,test$Completion)
  acc<-as.numeric(as.character(cmat$overall[1]))
  
  #Printing Confusion Matrix
  flog.info(predyear)
  flog.info(acc*100)
  
  #Printing Confusion Matrix
  print(paste('Confusion Matrix', as.character(predyear)))
  print(cmat)
  
  cacc<-c(cacc,acc)
  cyear<-c(cyear,predyear)
  
}

cacc<-cacc*100
barplot(cacc,ylim = c(0,100), main = 'Random Forest', ylab = 'Accuracy', names.arg = cyear)
abline(h = 50, col = 'red', lty = 6)

# tunedata<-data.frame(data$Admit_Term,data$Gender,data$hsgpa,
#                      data$SATMA,data$SATVB,data$Total_SAT,
#                      data$ACAD_GROUP_A,data$MajorType,data$Changed_Major,
#                      data$Honors,data$D_Adm,data$Distance2,
#                      data$Income2,data$Percentage,data$Population)
# 
# fgl.res <- tuneRF(tunedata, data$Completion,stepFactor = 2)
