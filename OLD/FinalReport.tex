\documentclass{article}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
\usepackage[linewidth=1pt]{mdframed}
\usepackage[colorlinks]{hyperref}

\usepackage{algorithm}
\usepackage{algpseudocode}

\usepackage{verbatim}

\hypersetup{citecolor=DeepPink4}
\hypersetup{linkcolor=DarkRed}
\hypersetup{urlcolor=Blue}

\usepackage{cleveref}

\setlength{\parindent}{1em}
\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.0}

\begin{document}

\begin{titlepage}
	\centering
	{\scshape\LARGE Project Report\par}
	\vspace{1cm}
	{\scshape\Large Admissions data\par}
	\vspace{1.5cm}
	{\Large\itshape \par}
	\vfill
	{\large \today\par}
\end{titlepage}

\section{Introduction}

\subsection{Background}

\textit{University of Massachusetts Dartmouth} gives financial aid to many students every year. According to the University's website, $86\%$ of the students that apply for financial aid at UMass Dartmouth end up receiving this requested aid. 

Some of the students that receive this aid will complete their studies here, at UMassD, but some other of them will \textbf{drop} or \textbf{transfer} to another school before completing their studies. This has the potential to be a big financial problem for the University, that sees how the time and money the University invested on these students, goes to waste. 

Our goal is to \textbf{help UMassD give financial aid to the right students}, which are the ones that will successfully complete their studies in our school.

\subsection{Project Goals}

As we stated before, our goal is to help UMassD give financial aid only to the right students. In order to achieve this goal we want to predict if a student will or will not complete his or her studies at our school. We used \textit{Supervised Machine Learning Algorithms}, including: Adaboost, Random Forest, Support Vector Machine (SVM) and Logistic Regression, on a dataset that contains different features from UMassD admitted students through 2000 to 2015. As this dataset contains a limited number of features, and an even more limited number of features that we can use, had to do some feature engineering to try to enrich our dataset with new features, thus increasing the quality of our dataset, to help our machine learning algorithms carry out their classification task. 

\section{Data}

The data we worked with on this project is, as we said before, a dataset containing UMass Dartmouth students' features that goes from 2000 all the way to 2015. This dataset contains around 25000 rows, and 20 features, including: Gender, High School GPA, SAT scores, \dots, \textbf{Completion} which will be our prediction variable. We were only able to use a limited number of features from this dataset, as we only uses the features that were available at the moment the student joined our school. Other features like: student graduation term, graduation program, etc. were scrapped, as they contained information that we won't have available when a student joined our school. 

The dataset that we used in this project is a real world dataset, therefore, we had to deal with a lot of \textbf{missing} and \textbf{incorrect} data. How we dealt with this data is related later in this report.

\subsection{Dataset Overview}

Prior to making any changes, we did some exploratory analysis with our dataset. This way we get to know better the data we are going to be working with in our project. 

The stacked barchart shown below represents the number of students admitted in UMassD from 2000 to 2015, and their distribution by college within UMassD. As we can see, the majority of the students are distributed in three main colleges, College of Arts and Sciences, Charlton College of Business and College of Engineering.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.25]{stacked1}
\end{center}
\caption{Number of students and students school by term}
\end{figure}

The barchart shown below represents the completion rate of students by admission term. The last 4 terms' completion rate is lower compared to the rest of the terms, but there is an easy explanation for this: the students admitted in these terms were still completing their studies when this dataset was created (2015).

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.3]{completion}
\end{center}
\caption{Completion rate by term}
\end{figure}

As shown in the piechart below almost all students in our school come from Massachusetts, this is really important, because as almost all students come from MA, the students' state of origin won't be such an important variable for our classification task, as there is no variance within it.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{state}
\end{center}
\caption{Students by State of origin}
\end{figure}

This last piechart represents the distribution by county of the students that come from MA.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{county}
\end{center}
\caption{Students by County of origin (MA)}
\end{figure}

\subsection{Data preprocessing}

As told before, we are dealing with a real-life dataset, we had to deal with a lot of missing and incorrect data then.

For the missing values in the dataset, we decided to go with the \textit{mean substitution}, so for all the \textbf{quantitative} values, we replaced the missing rows with the mean value of the whole column, excluding the missing values of course. In case of \textbf{categorical} values, there was nothing we could do, so we had to leave these values missing.

While doing the exploratory analysis of the dataset, we discovered that there was a lot of incorrect data. We were looking at the completion rate by term for the high schools that bring the most students to our school. During the 2000 to 2004 period, some of these high schools' students have a $100\%$ completion rate, and for some other high schools' students, the completion rate was $0\%$. In the end, as there was no way of fixing this data, we had to scrap it, so we ended up having a dataset that goes from 2004 to 2015 instead. We got rid of any possible duplicate rows in the dataset also.

Finally, in order to help our machine learning algorithms to better carry out their classification task, we normalized the data, using the \textit{min-max method}, so we can have all the quantitative values in a range that goes from 0 to 1.

$$z = \frac{x-min(x)}{max(x)-min(x)}$$

\subsection{Feature Engineering}

In this section, we will present the different features we engineered to enrich our dataset with more relevant features. The final goal of this feature engineering was to boost our machine learning algorithms' performance by adding different features, like location based features, demographics, etc.

\subsubsection{General}

We started by adding new simple features to our dataset, based on the features that were already present in the original dataset. 

\begin{itemize}
 \item \textbf{Student's Total SAT}
 $$SAT_{Total} = SAT_{Math} + SAT_{Verbal}$$
 \item Honorific Student Flag

 Based on the student's High School GPA

 \item \textbf{Above or below average student}
 
 Based on the admitted students' H.S. GPA and SAT
 
 \item \textbf{Student's Admission Flag}

 Differentiate between students that were directly admitted in one of the schools within UMassD, or admitted through pathway program
 
 \item \textbf{Student's origin}
 
 We differentiated between students from the same county (Bristol), students from the same state (MA), out-of-state students, and foreigners
\end{itemize}

\subsubsection{Student's Performance}

One feature that we thought might be helpful to our classification task was the student's performance. We wanted to find the answer to this question: \textit{Is a student a good/average/below average student?}. 

We divided the students into different performance groups, based on how the student's GPA and SAT compared to the GPA and SAT of students in the student's same county (we chose the student's county so we can have a big enough sample size). We ended up dividing the students into 3 different performance groups based on these selected features.

\begin{itemize}
	\item \textbf{Low performance} $\rightarrow$ Bottom 30\% of the students
	\item \textbf{Average performance} $\rightarrow$ Between bottom 30\% and top 30\% of the students
	\item \textbf{High performance} $\rightarrow$ Top 30\% of the students
\end{itemize}

\subsubsection{Distance Matrix API}

We used Google's \textit{Distance Matrix API} to find how far from our school a student lived before moving to UMassD. In order to obtain the distance in time and kms from the students high school to our school, we need to send a request to the distance matrix API that looks like this:

\begin{center}
\begin{verbatim}
https://maps.googleapis.com/maps/api/distancematrix/
json?units=imperial&origins=Washington,DC&destinations=New+York
\end{verbatim}
\end{center}

The API returns the data in \textit{JSON} format containing the distance in km and time, and we store it into our dataset.

Using the data obtained this way, we created another really simple derived feature. This feature differentiates between local students (students that live within 20 minutes from our school), and students that are not from our school's area.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.3]{dist}
\end{center}
\caption{}
\end{figure}

\subsubsection{Geocoding API}

We used Google's \textit{Geocoding API} to obtain each student's \textbf{zip-code}, as this feature wasn't available in the original dataset.

This API takes the students' high school name as an input, and returns the students' high school coordinates \textit{(longitude, latitude)} in \textit{JSON} format. Here is how a request for this API looks like:

\begin{center}
\begin{verbatim}
https://maps.googleapis.com/maps/api/geocode/
json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA
\end{verbatim}
\end{center}

We obtained and stored all the students' high school coordinates this way. 

Later, we used another dataset that contains all US's zip-codes and their corresponding coordinates to find which zip-code corresponds to each one of the students. We found the student's closest zip-code by first calculating the distance between a student's high school coordinates and all the zip-code dataset's coordinates, and by then taking the one with the minimum distance. 

As there might be incorrect values, because some students coordinates may not be correct, we limited the maximum distance between a student's coordinates and the zip-code coordinates to 30 miles, so we can easily get rid of these.

\subsubsection{American Community Survey API}

Once we obtained the student's zip-codes, we wanted to find demographic features related to those students' zip-codes. The question that we were trying to answer here was \textit{How does a student's background compare to the other students' background?}. 

In order to obtain the different demographic features, we used the \textit{American Community Survey API}. This API contains data from the American Community Survey, this survey is run every 5 years, and contains different demographic features from all Americans like: Unemployment Rate, Average Household Income, Population, etc. Here is how a request to this API looks like: 

\begin{center}
\begin{verbatim}
https://api.census.gov/data/2015/acs/acs5/profile?get=DP04_0001E,
NAME&for=zip%20code%20tabulation%20area:02461&key=keyname
\end{verbatim}
\end{center}

We sent a request to the API using student's zip-code we obtained before using Geo-coding. We obtained these different features from the ACS API.

\begin{itemize}
 \item \textbf{Unemployment Rate}
 \item \textbf{Education} (Percentage of population with a college degree or higher)
 \item \textbf{Poverty}
 \item \textbf{Average Household Income}
 \item \textbf{Population}
\end{itemize}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.1]{acs}
\end{center}
\caption{American Community Survey logo}
\end{figure}

\subsubsection{College Scorecard Data}

Another interesting feature that we thought may be helpful to our machine learning algorithms was a feature that compared our school to the other schools that offered the same programs as our school, in terms of ranking. We wanted have a feature that told us if our school was the students's first, second, or n choice. The question that we were trying to answer here was \textit{How does our school compare to other schools that offer the same programs as our school?}.  

We found a really interesting dataset, the \textit{College Scorecard Data}, issued by the US department of education, which is a dataset that contains features from all United States' colleges, like demographics from the students, the distribution of majors within the school, schools latitude and longitude, etc. Using this dataset, we could find the colleges that offered the same programs that our school offers.

First, we filtered this dataset, taking out the colleges that don't offer the same, or similar programs our school offers. We did this by manually classifying the majors that UMassD offers into the different groups available in the College Scorecard's dataset, and then taking from this dataset only the collegest that had students within these groups.

Once we had the colleges that offer the same or similar programs as our school, for each student, we further reduced the data to the schools that are within a 50 mile range from the student's high school. We found the schools within 50 miles by calculating the distance between the student's coordinates that we obtained before by \textit{geocoding}, and the coordinates available in the College Scorecard dataset. This way, we had the number of schools that offered the same or similar program as the students program that are within 50 miles of this student.

Finally, we had to find out how does UMassD compare to the other schools in terms of ranking, and this is where found our main issue. As for now, there is not an objective method on how to rank schools, as most of the rankings are based on features like prestige to rank the schools. We tried different features to rank the schools, like students' SAT, GPA, etc. but we ended up manually adding the ranking of each school obtained from the website \textit{www.usnews.com}. In the end UMassD would be the \textit{n-th} choice for a student, being $n$ the number of schools with a ranking higher than UMassD that offer the same majors as the student chose (we made the assumption that a student will always prefer a higher ranked school than UMassD, doesn't matter how pricy the school is).

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.07]{usdpt}
\end{center}
\caption{US Department of Education logo}
\end{figure}

\section{Machine Learning Algorithms}

In this section we will briefly introduce the different Supervised Machine Learning algorithms used in this project, and why we chose to use those algorithms.

\subsection{Adaboost}

Adaboost is a strong classifier that results from a linear combination of weak classifiers. At first, AdaBoost will initiallize a weight for every classifier, and with each new observation, if a classifier predicted the class correctly, this classifier's weight will be increased, otherwise the classifier's weight will be decreased.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=1]{adaboost}
\end{center}
\caption{Adaboost Overview}
\end{figure}

\subsection{Random Forest}

Random forest is a machine learning algorithm that uses multiple decision trees. Random Forest is a \textit{bagging} algorithm, each one of the trees in the Random Forest has a \textit{vote}, this vote defines the class each tree assigns to the observation. Then the class with most votes will be the predicted class (majority vote). 

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.5]{randomforest}
\end{center}
\caption{Random Forest Overview}
\end{figure}

\subsection{Support Vector Machine (SVM)}

SVM is a model suitable for binary classification. This model will try to find the best \textit{hyperplane} that separates all data points of one class from those on the other class, in our case students that completed the program and students that did not complete the program. The best hyperplane will be the hyperplane with the biggest margin between the two classes.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.3]{svm}
\end{center}
\caption{SVM Overview}
\end{figure}

\subsection{Logistic Regression}

Logistic regression is a form of regression that predicts discrete variables. Binary Logistic regression comes as a natural choice when the response variable is categorical with two possible outcomes, as it is in our case.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.5]{lreg}
\end{center}
\caption{Binary Logistic Regression Overview}
\end{figure} 

\section{Experimental Results}

In this section we will first present our experimental setting, and then we will show some of the results obtained from running the machine learning algorithms onto our dataset. Later we will analyze one of the machine learning algorithms (Random Forest) to obtain more insights about how these algorithms performed.

\subsection{Experimental Setting}

For running our tests, we used the dataset with all the features that we added by feature engineering the original dataset. Our goal with this tests was to simulate a real life scenario, so in order to select our training and test set we used a \textit{sliding window} which selected a year as the test set, and the two previous years as the training set. This means our training/test ratio was 2/1 close to the recommended 70/30.

For tuning our models, we used \textit{CARET's} package Grid Search, which takes a set of parameters as the input, and then it will train the model using all the different combinations of these parameters to find the parameters that best fit our classification task.

\subsection{Results}

In the barchart below you can see a side by side comparison of all the different models used in this project. At first it may seem like Logistic Regression and Random Forest are outperforming all the other models, but these results are misleading, as we will show later, using Random Forest as an example. The reason these models are outperforming the others is that these models are failing at classifying the students as completion or drop/transfer, only classifying students as non completing students, therefore as more students are not graduating we get that apparently 'good' performance.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{modelcomp}
\end{center}
\caption{Model side by side comparison}
\end{figure}

\subsection{Example - Random Forest}

In this section we will further analyze the results obtained in one of our machine learning algorithms over our datasets, so we can get a better understanding on how our models performed.

On the graph below we plotted the classification accuracy of the original dataset vs the classification accuracy of the new dataset with all the implemented features. As you can see, the model with our implemented features slightly outperforms the one that only uses the original data, so by feature engineering, we managed to slightly boost our models performance.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{newdata}
\end{center}
\caption{New dataset vs. old dataset}
\end{figure}

Then we plotted the error rate versus the number of trees, this plots represents the Class 1 classification error, the Class 0 classification error and the \textit{Out Of Bag} error, which represents the overall prediction error for Random Forest algorithm. The graph shows that we have a fairly good error for classifying students dropping or transferring to another school (Class 0), but our model really struggles for classifying students graduating in our school (Class 1). This can further be noticed on the confusion matrix.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{errate}
\end{center}
\caption{Error rate vs. Number of trees}
\end{figure}

As we stated before, this model has a hard time classifying students as Class 1, which can be noticed in the Confusion Matrix below. As we can see, the model is really biased towards Class 0 students, so almost every student in our test set is classified as Class 0. We tried to fix this bias using different methods, \textit{oversampling}, \textit{undersampling}, and synthetic data. Oversampling over samples the minority class, in our case Class 1 to balance the dataset. Under sampling under samples the majority class, in our case Class 0  to balance the dataset. The last method generates synthetic data from both classes to balance the dataset. None of these methods really helped to get rid of our models' bias.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{cmatrf}
\end{center}
\caption{Confusion Matrix (Random Forest)}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{rocrf}
\end{center}
\caption{ROC curve (Random Forest)}
\end{figure}

We also plotted the variable importance for Random Forest, which represents the most important variables for our classification task. In this plot, we are only showing the most important variables.

As you can see, the most important variables for our model are the quantitative variables related to student's performance in High School like high school GPA, the student's SAT scores (SATMA, SATVB and total SAT), and the distance the student lived from UMassD. Some other important variables are the demographic features that we obtained from the American Community Survey API like Avg. Household Income, Population, Poverty Rate, etc. Surprisingly, the college a student was admitted in, represented by \textit{ACAD\_GROUP\_A}, wasn't that important for our classification task, neither the major the student chose, as it isn't even represented in this graph.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.4]{varimport}
\end{center}
\caption{Variable Importance}
\end{figure}

\section{Conclusions}

As we saw the task of predicting student dropout/retention is not an easy one. We tried feature engineering to try to help our machine learning algorithms to better carry out this task, but as we saw in the results, the features that were originally available, and the features that we engineered were not relevant enough to our classification task, as these models are biased towards the majority class. 

We tried different machine learning algorithms, and we found out that although some algorithms may look like they are hugely outperforming others, if we look deeper we can see that these algorithms are highly biased and are not fit for the task we wanted to carry out. In order to fix this bias we tried different techniques to balance our training and test set class ratio, but this method didn't end up fixing this problem.

For now the model of our choice is AdaBoost, as even though is not the best overall performing model, it has a more balanced error between class 0 classification and class 1 classification, as we can see if we compare it with other models like Random Forest (see confusion matrix comparison below). We also tried between different models of AdaBoost, including: discrete Adaboost, real Adaboost, and gentle AdaBoost. The difference in terms of performance between these 3 models was insignificant, as you can see in the figure below, so we would stick to discrete AdaBoost for now.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=.25]{rfvsada}
\end{center}
\caption{Random forest vs. AdaBoost}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[scale=.55]{adacompare}
\end{center}
\caption{AdaBoost Comparison}
\end{figure}

\subsection{Future Work}

As we stated before, the features that we had available or that we engineered were not relevant enough, so our models, even though they had an OK at best performance, are far from something that can be implemented or used as for now. The dataset that we had available, even though it contained a fair amount of features, we could only use a limited number of them for our classification task, and within the variables that we were able to use, only a few of them were actually relevant. Because of this, what I propose is that in the future we tailor a data gathering system for all the students admitted in UMassD, this way we can cherry pick the variables that we think are helpful to our task like: Student's Age, Student's Zip-code, number of subjects the student enrolled into, etc. 

Another idea that would probably help the University to tackle the problem of students leaving the university would be to differentiate between the students that drop school, and the students that transfer out to another school. These two groups are completely different, and require completely different strategies to fix this problem, but it could help UMassD figure out why students are leaving, and look for a solution for this problem. This won't be an easy task neither, as it would be hard to find out if a student is dropping out, or if the student is actually transferring to another school.

\end{document}